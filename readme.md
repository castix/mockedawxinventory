# MockedAwxInventory

A Mock Implementation of AWX Inventory management API

Mimics some parts of the /inventories /groups /hosts management as described by the [docs v3.8.6](https://docs.ansible.com/ansible-tower/latest/html/towerapi/api_ref.html#/Inventories/Inventories_inventories_delete)

Nothing is stored permanently: it works purely in-memory.

## Rationale

Installing AAP/AWX/Tower is way too much complicated nowadays
(You may need a k8s/ocp cluster to use an operator, setup postgres and so on)
By using this mock you can skip all of that and focus on the inventory management itself.

## Dependencies
- Python3 and nothing else

## Executing

```bash
python3 server.py
```

## Additional(?) Constraints

- groups with the same name can go in different inventories
- hostnames and ip_addresses must be unique
- host creation is not idempotent (tells you if you're adding an host in the same group or not)
- provides an additional /print endpoint
- refuses to delete a non empty group or inventory

## Implemented Endpoints

| method | path                                      | description                                  |
|--------|-------------------------------------------|----------------------------------------------|
| get    | /print                                    | dump the current status                      |
| get    | /ap1/v2/inventories/?name=asd             | get the inventory with anme asd              |
| get    | /api/v2/inventories/{id}/groups/          | get the groups of the inventory              |
| get    | /api/v2/inventories/{id}/groups/?name=asd | get the group with name asd of the inventory |
| get    | /api/v2/inventories/{id}/hosts/           | get all hosts of inventory                   |
| get    | /api/v2/inventories/{id}/hosts/?name=asd  | get the host with name asd of the inventory  |
| get    | /api/v2/groups/{id}/hosts/                | get all hosts of group                       |
| post   | /api/v2/inventories/                      | create an inventory                          |
| post   | /api/v2/groups/                           | create a group                               |
| post   | /api/v2/groups/{group_id}/hosts/          | create an host and assign it to a group      |
| delete | /api/v2/groups/{id}                       | delete a group (if empty)                    |
| delete | /api/v2/inventories/{id}                  | delete an inventory (if empty)               |
| delete | /api/v2/hosts/{id}                        | delete an host                               |

## Limitations

- The upstream API might change at any time, please check it before using this
- The replies are not complete, feel free to share them back if you get them implemented
