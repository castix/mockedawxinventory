# Copyright (c) 2024 Vincenzo Castiglia
# Copyright (c) 2024 Par-Tec, S.p.A.
# Copyright (c) 2024 Red-Hat, Inc.
# Copyright (c) 2024 Terna, S.p.A.
from http.server import HTTPServer, HTTPStatus, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qsl
import json
import socket
import sys


class Inventory:
    def __init__(self):
        self.inventories = {}
        self.hosts = {}
        self._group_id = 0
        self._host_id = 0

    def _new_inventory_id(self):
        return str(max([int(_id) for _id in self.inventories.keys()] or [-1]) + 1)

    def _new_group_id(self):
        self._group_id += 1
        return str(self._group_id)

    def _new_host_id(self):
        self._host_id += 1
        return str(self._host_id)

    def create_inventory(self, name):
        _id = self._new_inventory_id()
        self.inventories[_id] = {
            'id': int(_id),
            'name': name,
            'groups': []
        }

    def create_group(self, body):
        groups = self.inventories[body.get('inventory')]['groups']
        groups.append({
            'id': int(self._new_group_id()),
            'name': body.get('name'),
        })

    def _group_by_id(self, group_id):
        for inv in self.inventories.values():
            for grp in inv.get('groups'):
                if str(grp.get('id')) == group_id:
                    return grp

    def _inventory_by_group_id(self, group_id):
        for inv in self.inventories.values():
            for group in inv.get('groups'):
                if str(group.get('id')) == group_id:
                    print(inv)
                    return inv
        raise ValueError(f'inventory of group {group_id} not found')

    def _where_is_host_with_id(self, host_id):
        for inventory_id, inv in self.inventories.items():
            for group in inv.get('groups'):
                for hostname in group.get('__hosts', []):
                    host = self.hosts[hostname]
                    if str(host.get('id')) == host_id:
                        return inventory_id, inv.get('groups').index(group), hostname
        raise ValueError(f'host with id {host_id} not found')

    def _check_ip_already_present(self, ip_address, name):
        for host in self.hosts.values():
            if host['variables'].get('ansible_host') == ip_address:
                raise ValueError(f'ip_address {ip_address} {name} duplicate of host {host["name"]}')

    def create_host(self, group_id, name, description='', variables=None):
        if variables is None:
            variables = {}
        if variables.get('ansible_host'):
            self._check_ip_already_present(variables.get('ansible_host'), name)
        group = self._group_by_id(group_id)
        if name in group.get('__hosts', []):
            raise ValueError('hostname already in this group')
        group.setdefault('__hosts', []).append(name)
        if name in self.hosts:
            raise ValueError('hostname already present elsewhere')
        inventory = self._inventory_by_group_id(group_id)
        self.hosts[name] = {
            'id': self._new_host_id(),
            'name': name,
            'description': description,
            'inventory': inventory.get('id'),
            'variables': variables,
            'summary_fields': {
                # i'm unable to find ansible_base.lib.utils.models source code
                # used by the awx serializers...
                'inventory': {
                    'id': inventory.get('id'),
                    'name': inventory.get('name'),
                    'total_groups': len(inventory.get('groups', [])),
                    'total_hosts': sum(
                        len(grp.get('__hosts', []))
                        for grp in inventory.get('groups')
                    ),
                },
                'groups': {
                    'count': 1,
                    'results': [{
                        'id': group_id,
                        'name': self._group_by_id(group_id),
                    }],
                },
            },
        }

    def delete_host(self, _id):
        inventory_name, group_index, hostname = self._where_is_host_with_id(_id)
        self.inventories[inventory_name]['groups'][group_index]['__hosts'].remove(hostname)
        del self.hosts[hostname]

    def delete_group(self, _id):
        for inventory in self.inventories.values():
            for group in inventory['groups']:
                if str(group.get('id')) == _id:
                    if len(group.get('__hosts', [])):
                        raise ValueError('refusing to delete a non-empty group')
                    inventory['groups'].remove(group)

    def delete_inventory(self, _id):
        inventory = self.inventories.get(_id)
        for grp in inventory.get('groups'):
            if grp.get('__hosts'):
                raise ValueError('refusing to delete a non-empty inventory')
        del self.inventories[_id]

    def inventory_by_name(self, name):
        for inv in self.inventories.values():
            if inv.get('name') == name:
                return [inv]
        return []

    def groups_of_inventory(self, inventory_id):
        for inv_id, inv in self.inventories.items():
            if inv_id == inventory_id:
                return inv.get('groups')
        return []

    def hosts_of_inventory(self, inventory_id):
        hosts = []
        for group in self.groups_of_inventory(inventory_id):
            for hostname in group.get('__hosts', []):
                hosts.append(self.hosts.get(hostname))
        print(hosts)
        return hosts

    def host_of_inventory(self, inventory_id, hostname):
        return [
            host
            for host in self.hosts_of_inventory(inventory_id)
            if host.get('name') == hostname
        ]

    def group_by_name_through_inventory(self, inventory_id, group_name):
        for inv_id, inv in self.inventories.items():
            if inv_id == inventory_id:
                return list(filter(
                    lambda grp: grp.get('name') == group_name,
                    inv.get('groups')
                ))
        return []

    def hosts_of_group_by_id(self, group_id):
        for inventory in self.inventories.values():
            for group in inventory['groups']:
                if str(group.get('id')) == group_id:
                    return [
                        self.hosts.get(h)
                        for h in group.get('__hosts', [])
                    ]
        raise ValueError('group with id', group_id, 'not found')

    

inventory = Inventory()


class MockAwxRequestHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.0'

    def handle_one_request(self):
        try:
            super().handle_one_request()
        except BaseException as exc:
            import traceback
            traceback.print_exc()
            self.send_response(HTTPStatus.INTERNAL_SERVER_ERROR)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(
                json.dumps(
                    {
                        'code': 500,
                        'message': 'generic internal server error',
                        'detail': str(exc)
                    },
                    indent=4,
                ).encode('ascii', 'ignore')
            )

    def get_dispatch(self, path):
        parsed_url = urlparse(path)
        query_params = dict(parse_qsl(parsed_url.query))
        if path == '/print':
            return {
                'inventories': inventory.inventories.copy(),
                'hosts': inventory.hosts.copy(),
            }
        if parsed_url.path.startswith('/api/v2/inventories/'):
            if '/api/v2/inventories/' == parsed_url.path:
                return {
                    'results': inventory.inventory_by_name(query_params.get('name'))
                }
            inventory_id = parsed_url.path.split('/')[4]
            if parsed_url.path.endswith('/groups/'):
                if query_params.get('name'):
                    return {
                        'results': inventory.group_by_name_through_inventory(
                            inventory_id,
                            query_params.get('name'),
                        )
                    }
                return {'results': inventory.groups_of_inventory(inventory_id)}
            elif parsed_url.path.endswith('/hosts/'):
                if query_params.get('name'):
                    return {
                        'results': inventory.host_of_inventory(inventory_id, query_params.get('name'))
                    }
                return {'results': inventory.hosts_of_inventory(inventory_id)}
        elif parsed_url.path.startswith('/api/v2/groups/'):
            group_id = parsed_url.path.split('/')[4]
            section = parsed_url.path.split('/')[5]
            if section == 'hosts':
                return {'results': inventory.hosts_of_group_by_id(group_id)}

    def post_dispatch(self, path, body):
        if path == '/api/v2/inventories/':
            inventory.create_inventory(body.get('name'))
        if path == '/api/v2/groups/':
            inventory.create_group(body)
        elif path.startswith('/api/v2/groups/'):
            group_id = path.split('/')[4]
            section = path.split('/')[5]
            if section == 'hosts':
                inventory.create_host(
                    group_id,
                    body.get('name'),
                    description=body.get('description', ''),
                    variables=body.get('variables'),
                )
        return {}

    def delete_dispatch(self, path):
        kind = self.path.split('/')[3]
        _id = self.path.split('/')[4]
        if kind == 'groups':
            inventory.delete_group(_id)
        elif kind == 'inventories':
            inventory.delete_inventory(_id)
        elif kind == 'hosts':
            inventory.delete_host(_id)
        else:
            raise NotImplementedError(f'deletion of {kind} not implemented')

    def do_GET(self):
        print('got GET on', self.path)
        result = self.get_dispatch(self.path)
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(
            json.dumps(result, indent=4).encode('ascii', 'ignore')
        )

    def do_POST(self):
        assert self.headers['Content-Type'] == 'application/json',\
            'Content-Type not application/json'
        # assert self.headers['Accept'] == 'application/json', \
        #     'Accept not application/json'
        body = self.rfile.read(int(self.headers['Content-Length']))
        jbody = json.loads(body.decode('utf-8'))

        print('got POST on', self.path, jbody)
        result = self.post_dispatch(self.path, jbody)
        self.send_response(HTTPStatus.CREATED)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(
            json.dumps(result, indent=4).encode('ascii', 'ignore')
        )

    def do_DELETE(self):
        self.delete_dispatch(self.path)
        self.send_response(HTTPStatus.NO_CONTENT)
        self.end_headers()
        self.wfile.write(b'')
        

class MockAwxServer:
    
    
    def serve_forever(self):
        address = ('', 8000)
        with HTTPServer(address, MockAwxRequestHandler) as httpd:
            host, port = httpd.socket.getsockname()[:2]
            url_host = f'[{host}]' if ':' in host else host
            print(
                f"Serving HTTP on {host} port {port} "
                f"(http://{url_host}:{port}/) ..."
            )
            try:
                httpd.serve_forever()
            except KeyboardInterrupt:
                print("\nKeyboard interrupt received, exiting.")
                sys.exit(0)

if __name__ == '__main__':
    server = MockAwxServer()
    server.serve_forever()
